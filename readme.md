# Mukuru assessment

## Installation

This project is built on Laravel 5.2 PHP framework. There are a few requirements for it to run properly:

    Apache
    PHP >= 5.5.9
    OpenSSL PHP Extension
    PDO PHP Extension
    Mbstring PHP Extension
    Tokenizer PHP Extension

The first step is to clone the project onto the computer you would like to run it on. Please execute the following
 command in a folder of your choice

    git clone git@bitbucket.org:ivaylosp/mukuru-currency-converter.git
        
Next we have to copy the **.env.example**  file to **.env**

Once the file is copied we have to edit the **.env** file and enter all the settings that are used by this project:

* Email settings : MAIL_DRIVER can be set to "smtp", "log" or "mailgun" if set to log all the send emails will be appended to the
logfile located under **ProjectFolder/storage/logs/laravel.log**. This is the recommended driver to use to ignore problems around
domain restrictions and signing up with 3rd parties. However if you like to use the smtp or mailgun options just enter the
settings accordingly.


* Database settings: You will have to create a database and enter all the appropriate settings in the .env file. All tables will
be created later on.


* Foreign currency exchange rate API: MUKURU_APILAYER_KEY please enter the key for APILAYER if you like to use that service.
Note that this service is only available if you are using version 2 of the API. To change the API version you can edit
**ProjectFolder/public/js/custom.js** file and change the **var api_version = 2;** This is the only place you will need to adjust
the API version since the calls to the API are done via AJAX


* MUKURU_EMAIL_FROM, MUKURU_EMAIL_TO, MUKURU_EMAIL_TO_NAME these settings are to do with the sending of the order emails. They are
required but there are no checks in place to prevent you from running the software without them as it is out of scope.


Once we are done with all the settings we can run the following commands:

There is a version of composer included with this repository but if you have composer globaly installed will be ok.
Once cloned please go to the directory of the project and install all the composer libraries using the following commands

Using local composer (the chmod is necessary if you are using the local copy):
    
    chmod 777 composer
    ./composer install    

Or using global composer:

    composer install

If you do not have composer please visit the following website for setup instructions.

[https://getcomposer.org/](https://getcomposer.org/)

Please execute the following command to generate a new application key

    php artisan key:generate

The following will create all the necessary tables in the database.

    php artisan migrate

Please note that the currency rate cache is updated by a scheduled job which should be setup behing a CRON but also 
can be called with the following command:

    php artisan "currency-rate-update"

Please run this command at least once otherwise the v2 of the API will serve default exchange rates. Make sure you have
specified the MUKURU_APILAYER_KEY in the .env file or you will get an error running the command.

Last step is to point your apache vhost to the **ProjectFolder/public** folder and load the project. 

##API Version information

###Version Switching
    * Version switching happens trough a header variable "api-version". Currently available versions are 1 and 2
     
     
###V1
    * Currency exchange rates are hardcoded
    
    
###V2
    * Currency exchange rates are fetched from 3rd party API and cached in database. They get refreshed using a scheduled task and there is a fallback
    to hardcoded rates if the fetching of the rates fails.


## Note

    * Please note that none of the currencies or calculations are rounded as i am not sure the correct business logic around
    the rounding.
    
    * The application has to be running form a http://example.dev host and not from http://example.dev/folder/ due to
    routing problems that will surface if its run from a folder.