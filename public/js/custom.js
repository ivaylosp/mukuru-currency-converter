jQuery(document).ready(function(){

    var api_version = 2;

    jQuery('#convert_button').click(function(){
        jQuery.ajax({
            method: 'get',
            dataType: 'json',
            url: "/api/currency-converter",
            headers: { 'api-version': api_version },
            accepts: "application/json; charset=utf-8;",
            data: {
                currency_type   : jQuery('#currency').val(),
                usd_amount      : jQuery('#usd_amount').val(),
                currency_amount : jQuery('#currency_amount').val()
            },
            success: function(data) {
                jQuery('#quote_info').html('');
                jQuery('#quote_container').show();
                jQuery.each(data, function(index, val) {
                    var label = index.replace('_',' ').capitalize();
                    jQuery('#quote_info').append(document.createTextNode(label + " - " + val + "\n"));
                });
                jQuery('#quote_info').html(jQuery('#quote_info').html().replace(/\n/g, "<br />"));
                jQuery('.alert').removeClass('alert-danger').addClass('alert-success').html('Conversion Completed');
            },
            error: function(xhr, status, error) {
                jQuery('.alert').html('');
                jQuery('.alert').removeClass('alert-success').addClass('alert-danger');
                var data = jQuery.parseJSON(xhr.responseText);
                jQuery.each(data, function(index, val) {
                    jQuery('.alert').append(val + "\n");
                });
                jQuery('.alert').html(jQuery('.alert').html().replace(/\n/g, "<br />"));
            }
        });
    });

    jQuery('#order_button').click(function(){
        jQuery.ajax({
            method: 'post',
            dataType: 'json',
            url: "/api/order",
            headers: {'api-version': api_version},
            accepts: "application/json; charset=utf-8;",
            data: {
                currency_type: jQuery('#currency').val(),
                usd_amount: jQuery('#usd_amount').val(),
                currency_amount: jQuery('#currency_amount').val()
            },
            success: function(data) {
                jQuery('#quote_info').html('Thank you for placing an order!');
            },
            error: function(xhr, status, error) {
                jQuery('.alert').html('');
                jQuery('#quote_container').hide();
                jQuery('.alert').removeClass('alert-success').addClass('alert-danger');
                var data = jQuery.parseJSON(xhr.responseText);
                jQuery.each(data, function(index, val) {
                    jQuery('.alert').append(val + "\n");
                });
                jQuery('.alert').html(jQuery('.alert').html().replace(/\n/g, "<br />"));
            }
        });
    });

    jQuery('#usd_amount').on('focus',function(){
        jQuery('#currency_amount').val('');
        jQuery('#quote_container').hide();
    });

    jQuery('#currency_amount').on('focus',function(){
        jQuery('#usd_amount').val('');
        jQuery('#quote_container').hide();
    });

    jQuery('#currency').on('change',function(){
        jQuery('#convert_button').click();
    });
});

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}