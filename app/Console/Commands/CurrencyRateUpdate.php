<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mukuru\v2\Services\CurrencyRateService;

class CurrencyRateUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'currency-rate-update';

    /**
     * The console command description.
     *
     * @var string
     */

    protected $description = 'Migrate all images from local storage to S3 bucket and update database to reflect';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(CurrencyRateService $currencyRateService)
    {
        $currencyRateService->fetchRates();
    }
}
