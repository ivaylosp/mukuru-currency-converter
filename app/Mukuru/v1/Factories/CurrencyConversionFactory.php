<?php

namespace Mukuru\v1\Factories;

class CurrencyConversionFactory {

    public static function create($className)
    {

        $className = '\\Mukuru\\v1\\Services\\'.$className;

        if(class_exists($className)){
            return new $className();
        }else{
            return false;
        }
    }

}