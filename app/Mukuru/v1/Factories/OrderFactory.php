<?php

namespace Mukuru\v1\Factories;

class OrderFactory {

    public static function create($className)
    {

        $className = '\\Mukuru\\v1\\Services\\'.$className;

        if(class_exists($className)){
            return new $className();
        }else{
            return false;
        }
    }

}