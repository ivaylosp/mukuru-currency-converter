<?php

namespace Mukuru\v1\Services;

use Mukuru\v1\Interfaces\CurrencyConversionInterface;

class KESService implements CurrencyConversionInterface {

    public function getConversion($data){

        if($data['usd_amount']){
            $usdAmount = $data['usd_amount'];
        }else{
            $usdAmount = $this->getCurrencyToUsd($data['currency_amount']);
        }

        $surCharge = $this->getSurchargeTotal($usdAmount);

        $object = new \stdClass();
        $object->currency_type = 'KES';
        $object->exchange_rate = $this->getCurrencyRate();
        $object->surcharge_percentage = $this->getSurchargePercentage();
        $object->currency_amount = $this->getUsdToCurrency($usdAmount);
        $object->surcharge_amount = $surCharge;
        $object->usd_amount = $usdAmount + $surCharge;

        return $object;
    }

    public function getUsdToCurrency($usdAmount){
        return $usdAmount * $this->getCurrencyRate();
    }

    public function getCurrencyToUsd($currency){
        return $currency / $this->getCurrencyRate();
    }

    private function getCurrencyRate()
    {
        return 103.860;
    }

    private function getSurchargePercentage(){
        return 0.025;
    }

    private function getSurchargeTotal($usdAmount)
    {
        return $usdAmount * $this->getSurchargePercentage();
    }
}