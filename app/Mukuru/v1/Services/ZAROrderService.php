<?php

namespace Mukuru\v1\Services;

use Illuminate\Mail\Mailer;
use Mukuru\v1\Order;
use Mukuru\v1\Interfaces\OrderInterface;

class ZAROrderService implements OrderInterface {
    public function create($conversion){

        $conversion->usd_total_amount = $conversion->usd_amount + $conversion->surcharge_amount;

        $orderModel = new Order();
        $orderModel->fill((array)$conversion);
        $orderModel->save();

        return true;
    }
    public function sendEmail($data){
        //Not Implemented
        return true;
    }
}