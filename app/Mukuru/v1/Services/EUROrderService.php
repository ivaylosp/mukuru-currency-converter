<?php

namespace Mukuru\v1\Services;

use Mail;
use Mukuru\v1\Order;
use Mukuru\v1\Interfaces\OrderInterface;

class EUROrderService implements OrderInterface {

    public function create($conversion){

        $conversion->usd_total_amount = $conversion->usd_amount + $conversion->surcharge_amount;
        $conversion->usd_total_amount = $conversion->usd_total_amount - $conversion->usd_total_amount * 0.02;

        //This is an array of notes that can be send to email and saved in the database for future reference
        $conversion->special_notes[] = "Applied discount of 2% on the total amount of USD.";
        $conversion->special_notes = json_encode($conversion->special_notes);

        $orderModel = new Order();
        $orderModel->fill((array)$conversion);
        $orderModel->save();

        return true;
    }
    public function sendEmail($data){
        //Not Implemented
        return true;
    }

}