<?php

namespace Mukuru\v1\Interfaces;

interface CurrencyConversionInterface
{
    public function getUsdToCurrency($usdAmount);
    public function getCurrencyToUsd($currency);
}