<?php

namespace Mukuru\v1\Interfaces;

interface OrderInterface
{
    public function create($usdAmount);
    public function sendEmail($data);
}