<?php

namespace Mukuru\v1\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

use Mukuru\v1\Factories\CurrencyConversionFactory;

class CurrencyConversionController extends Controller
{

    protected $currencyConversionFactory;
    protected $orderModel;

    public function __construct(CurrencyConversionFactory $currencyConversionFactory)
    {
        $this->currencyConversionFactory = $currencyConversionFactory;
    }

    public function getConversion(Request $request)
    {
        $code = 200;

        $currencyAmount = $request->get('currency_amount');
        $usdAmount = $request->get('usd_amount');
        $currencyType = $request->get('currency_type');

        $data = $request->all();

        $rules = [
            'usd_amount' => 'numeric|present|required_if:currency_amount,',
            'currency_amount' => 'numeric|present',
            'currency_type' => 'in:ZAR,GBP,EUR,KES|required',
        ];

        $messages = [
            'usd_amount.required_if' =>'Please enter at least one of the currency options',
            'usd_amount.numeric' => 'Please enter a valid numeric value',
            'currency_amount.numeric' => 'Please enter a valid numeric value',
            'currency_type.in' => 'The allowed currency types are "ZAR, GBP, EUR, KES"',
        ];

        $this->validate($request, $rules, $messages);

        $currencyConversionService = $this->currencyConversionFactory->create(strtoupper($currencyType).'Service');

        if($response = $currencyConversionService->getConversion($data)) {

        }else{
            $response = new \stdClass();
            $code = 400;
        }

        return (new Response(json_encode($response), $code))
            ->header('Content-Type', 'application/json');
    }
}
