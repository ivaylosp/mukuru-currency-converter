<?php

namespace Mukuru\v2\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

use Mukuru\v2\Factories\CurrencyConversionFactory;
use Mukuru\v2\Factories\OrderFactory;

class OrderController extends Controller
{

    protected $currencyConversionFactory;
    protected $orderFactory;

    public function __construct(CurrencyConversionFactory $currencyConversionFactory, OrderFactory $orderFactory){
        $this->currencyConversionFactory = $currencyConversionFactory;
        $this->orderFactory = $orderFactory;
    }

    public function createOrder(Request $request)
    {
        $code = 200;

        $currencyAmount = $request->get('currency_amount');
        $usdAmount = $request->get('usd_amount');
        $currencyType = $request->get('currency_type');

        $data = $request->all();

        $rules = [
            'usd_amount' => 'numeric',
            'currency_amount' => 'numeric',
            'currency_type' => 'in:ZAR,GBP,EUR,KES',
        ];

        $messages = [
            'usd_amount.numeric' => 'Please enter a valid numeric value',
            'currency_amount.numeric' => 'Please enter a valid numeric value',
            'currency_type.in' => 'The allowed currency types are "ZAR, GBP, EUR, KES"',
        ];

        $this->validate($request, $rules, $messages);

        $currencyConversionService = $this->currencyConversionFactory->create(strtoupper($currencyType).'Service');
        $orderService = $this->orderFactory->create(strtoupper($currencyType).'OrderService');

        $conversion = $currencyConversionService->getConversion($data);
        $order = $orderService->create($conversion);

        if($conversion && $order) {
            $response = true;
        }else{
            $response = new \stdClass();
            $code = 400;
        }

        return (new Response(json_encode($response), $code))
            ->header('Content-Type', 'application/json');
    }

}
