<?php

namespace Mukuru\v2\Interfaces;

interface OrderInterface
{
    public function create($usdAmount);
    public function sendEmail($data);
}