<?php

namespace Mukuru\v2\Interfaces;

interface CurrencyConversionInterface
{
    public function getUsdToCurrency($usdAmount);
    public function getCurrencyToUsd($currency);
}