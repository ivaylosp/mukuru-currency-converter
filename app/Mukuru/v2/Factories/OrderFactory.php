<?php

namespace Mukuru\v2\Factories;

class OrderFactory {

    public static function create($className)
    {

        $className = '\\Mukuru\\v2\\Services\\'.$className;

        if(class_exists($className)){
            return new $className();
        }else{
            return false;
        }
    }

}