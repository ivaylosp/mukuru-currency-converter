<?php

namespace Mukuru\v2\Factories;

class CurrencyConversionFactory {

    public static function create($className)
    {

        $className = '\\Mukuru\\v2\\Services\\'.$className;

        if(class_exists($className)){
            return new $className(null);
        }else{
            return false;
        }
    }

}