<?php

namespace Mukuru\v2;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'currency_type',
        'exchange_rate',
        'surcharge_percentage',
        'currency_amount',
        'usd_amount',
        'surcharge_amount',
        'usd_total_amount',
        'special_notes'
    ];
}
