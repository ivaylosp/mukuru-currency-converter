<?php

namespace Mukuru\v2\Services;

use Mukuru\v2\Order;
use Mukuru\v2\Interfaces\OrderInterface;

class GBPOrderService implements OrderInterface {
    public function create($conversion){

        $conversion->usd_total_amount = $conversion->usd_amount + $conversion->surcharge_amount;

        $orderModel = new Order();
        $orderModel->fill((array)$conversion);
        $orderModel->save();

        $this->sendEmail($conversion);

        return true;
    }
    public function sendEmail($data)
    {
        \Mail::send('emails.order',(array)$data , function ($m){
            $m->from(env('MUKURU_EMAIL_FROM'), 'Mukuru')
                ->to(env('MUKURU_EMAIL_TO'), env('MUKURU_EMAIL_TO_NAME'))
                ->subject('Thank you for your order');
        });
    }
}