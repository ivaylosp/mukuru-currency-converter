<?php

namespace Mukuru\v2\Services;

use Mukuru\v2\CurrencyRate;

class CurrencyRateService {

    protected $currencyRateModel;

    public function __construct(CurrencyRate $currencyRateModel)
    {
        $this->currencyRateModel = $currencyRateModel;
    }

    public function fetchRates(){
        $api_key = env('MUKURU_APILAYER_KEY');
        $json = json_decode(file_get_contents("http://apilayer.net/api/live?access_key={$api_key}&currencies=ZAR,EUR,GBP,KES&format=1"), true);
        foreach($json['quotes'] as $key => $quote) {

            $currency_type = str_replace($json['source'],'',$key);

            $CurrencyRateModel = $this->currencyRateModel->newInstance();

            $result = $CurrencyRateModel->find($currency_type);

            if($result) {
                $result->exchange_rate = $quote;
                $result->save();
            }else{
                $CurrencyRateModel->currency_type = $currency_type;
                $CurrencyRateModel->exchange_rate = $quote;
                $CurrencyRateModel->save();
            }

        }
    }
}