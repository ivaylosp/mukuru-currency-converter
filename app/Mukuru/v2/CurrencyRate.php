<?php

namespace Mukuru\v2;

use Illuminate\Database\Eloquent\Model;

class CurrencyRate extends Model
{

    protected $primaryKey = 'currency_type';

    protected $fillable = [
        'currency_type',
        'exchange_rate'
    ];
}
