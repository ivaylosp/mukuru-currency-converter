<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$version = Request::header('api-version', '1');

Route::group(['prefix' => 'api', 'namespace' => 'Mukuru\v'.$version.'\Controllers'], function() {
    Route::post('order', 'OrderController@createOrder');
    Route::get('currency-converter', 'CurrencyConversionController@getConversion');
});

Route::get('/', function () {
    return view('currency-converter');
});