<html>
    <h3>Mukuru Order summary</h3>
    <table>
        <tbody>
            @if(@$special_notes)
            <tr>
                <td colspan="2">
                    @foreach(@$special_notes as $note)
                        {{$note.'<br />'}}
                    @endforeach
                </td>

            </tr>
            @endif
            <tr>
                <td>Currency type</td>
                <td>{{@$currency_type}}</td>
            </tr>
            <tr>
                <td>Exchange rate</td>
                <td>{{@$exchange_rate}}</td>
            </tr>
            <tr>
                <td>Surcharge percentage</td>
                <td>{{@$surcharge_percentage}}</td>
            </tr>
            <tr>
                <td>Currency amount</td>
                <td>{{@$currency_type}} {{@$currency_amount}}</td>
            </tr>
            <tr>
                <td>Usd amount</td>
                <td>USD {{@$usd_amount}}</td>
            </tr>
            <tr>
                <td>Surcharge amount</td>
                <td>USD {{@$surcharge_amount}}</td>
            </tr>
            <tr>
                <td>Total amount</td>
                <td>USD {{@$usd_total_amount}}</td>
            </tr>
        </tbody>
    </table>
</html>