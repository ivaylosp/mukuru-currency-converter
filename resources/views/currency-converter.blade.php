@extends('layouts.default')

@section('content')
    <div class="col-lg-12 ">
        <h1>Currency Converter</h1>
        <div class="alert" role="alert"></div>
    </div>
    <div class="col-lg-5 ">
        <div class="form-group">
            <label for="exampleInputEmail1">Please enter amount of USD you would like to pay</label>
            <input type="text" name="usd_amount" class="form-control" id="usd_amount" placeholder="USD Amount">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Please enter amount of foreign currency you would like to purchase</label>
            <input type="text" name="currency_amount" class="form-control" id="currency_amount" placeholder="Foreign Currency Amount">
        </div>
        <div class="form-group">
            <select name="conversion_option" id="currency">
                <option selected="selected" value="ZAR">ZAR</option>
                <option value="GBP">GBP</option>
                <option value="EUR">EUR</option>
                <option value="KES">KES</option>
            </select>
        </div>
        <button type="submit" class="btn btn-default" id="convert_button">Convert</button>
    </div>
    <div id="quote_container" class="col-lg-7" style="display:none;">
        <label for="">Currency conversion summary</label>
        <div class="form-group" id="quote_info"></div>
        <button type="submit" class="btn btn-default" id="order_button">Place Order</button>
    </div>
@endsection