<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('currency_type',['ZAR','GBP','EUR','KES']);
            $table->double('exchange_rate',15,8);
            $table->double('surcharge_percentage',15,8);
            $table->double('currency_amount',15,8);
            $table->double('usd_amount',15,8);
            $table->double('surcharge_amount',15,8);
            $table->double('usd_total_amount',15,8);
            $table->text('special_notes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
